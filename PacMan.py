from livewires import games, color
import math

games.init(screen_width = 800, screen_height = 600, fps = 50)


class Moving_Object(games.Sprite):
    def update(self):
        if self.top <= 5:
            self.top = 6

        if self.left <= 5:
            self.left = 6

        if self.right >= (games.screen.width - 5):
            self.right = (games.screen.width - 6)

        if self.bottom >= (games.screen.height - 5):
            self.bottom = (games.screen.height - 6)



class Ghost(Moving_Object):
    image = games.load_image("Ghost.gif")

    def __init__(self):
        super(Ghost,self).__init__(image = self.image,
                                   x = 780,
                                   y = 20,
                                   dx = 0,
                                   dy = 0)
        


    def update(self):
        super(Ghost,self).update()
        if self.x == 780 and self.y == 20: 
            self.dx = -1
            self.dy = 0
        





class Wall(Moving_Object):
    """Klasa ściany w grze"""
    wall = games.load_image("Wall.jpg")






    def __init__(self,x,y):
        super(Wall,self).__init__(image = self.wall,
                                  x = x,
                                  y = y)
        self.x = x
        self.y = y









class PacMan(Moving_Object):
    """Klasa duszka którym steruje gracz"""
    image = games.load_image("PMAN.png")

    def __init__(self):
       super(PacMan,self).__init__(image = PacMan.image,
                                   x = 20,
                                   y = games.screen.height - 20)




    
    def check_collidate_W(self,Ly1,Ly2,Lx1,Lx2):
        """funckja która blokuje przejscie duszka przez teksture"""
        if self.right in range(Lx1,Lx2) and self.y in range(Ly1,Ly2):
            self.x = (Lx1 - 16)
        
        if self.bottom in range(Ly1,Ly2) and self.x in range(Lx1,Lx2):
            self.bottom = (Ly1 - 1)

        if self.left in range(Lx1,Lx2) and self.y in range(Ly1,Ly2):
            self.left = (Lx2 - 1)

        if self.top in range(Ly1,Ly2) and self.x in range(Lx1,Lx2):
            self.top = (Ly2 - 1)




    #aktualizacja co klatke
    def update(self):
        """aktualizacja co klatke"""
        super(PacMan,self).update()
    
        self.check_collidate_W(30,287,36,72)
        self.check_collidate_W(30,287,102,138)
        self.check_collidate_W(317,566,36,72)
        self.check_collidate_W(317,566,102,138)





        if games.keyboard.is_pressed(games.K_w):
            self.angle = 270
            self.y -= 1


        if games.keyboard.is_pressed(games.K_d):
            self.angle = 0
            self.x += 1

        if games.keyboard.is_pressed(games.K_s):
            self.angle = 90
            self.y += 1

        if games.keyboard.is_pressed(games.K_a):
            self.angle = 180
            self.x -= 1
    
 

def main():
    background = games.load_image("background.png", transparent=False)
    games.screen.background = background
    pman = PacMan()
    gman = Ghost()


    LeftUPW1 = Wall(54,160)
    LeftUPW2 = Wall(120,160)
    #LeftUPW3 = Wall(187,160)
    #LeftUPW4 = Wall(253,160)
    #LeftUPW5 = Wall(319,160)
    #LeftUPW6 = Wall(385,160)
    #LeftUPW7 = Wall(451,160)
    #LeftUPW8 = Wall(517,160)
    #LeftUPW9 = Wall(583,160)
    #LeftUPW10 = Wall(649,160)
    #LeftUPW11 = Wall(715,160)
    #LeftUPW12 = Wall(781,160)
    LeftDownW1 = Wall(54,439)
    LeftDownW2 = Wall(120,439)
    #LeftDownW3 = Wall(187,439)
    #LeftDownW4 = Wall(253,439)
    #LeftDownW5 = Wall(319,439)
    #LeftDownW6 = Wall(385,439)
    #LeftDownW7 = Wall(451,439)
    #LeftDownW8 = Wall(517,439)
    #LeftDownW9 = Wall(583,439)
    #LeftDownW10 = Wall(649,439)
    #LeftDownW11 = Wall(715,439)
    #LeftDownW12 = Wall(781,439)
    games.screen.add(LeftUPW1)
    games.screen.add(LeftUPW2)
    #games.screen.add(LeftUPW3)
    #games.screen.add(LeftUPW4)
    #games.screen.add(LeftUPW5)
    #games.screen.add(LeftUPW6)
    #games.screen.add(LeftUPW7)
    #games.screen.add(LeftUPW8)
    #games.screen.add(LeftUPW9)
    #games.screen.add(LeftUPW10)
    #games.screen.add(LeftUPW11)
    #games.screen.add(LeftUPW12)

    games.screen.add(LeftDownW1)
    games.screen.add(LeftDownW2)
    #games.screen.add(LeftDownW3)
    #games.screen.add(LeftDownW4)
    #games.screen.add(LeftDownW5)
    #games.screen.add(LeftDownW6)
    #games.screen.add(LeftDownW7)
    #games.screen.add(LeftDownW8)
    #games.screen.add(LeftDownW9)
    #games.screen.add(LeftDownW10)
    #games.screen.add(LeftDownW11)
    #games.screen.add(LeftDownW12)
    games.screen.add(pman)
    games.screen.add(gman)
    games.screen.mainloop()

main()